<?php
$requestMethod = $_SERVER['REQUEST_METHOD'];

$arrValues = array();

if ($requestMethod == 'PUT'){
    
    parse_str(file_get_contents("php://input"), $arrValues);

    $arrValues['status_code'] = 0;
    $arrValues['status_description'] = 'No error found';
    
    $arrValues['name'] = 'Mr. ' . $arrValues['name'];
    $arrValues['address'] = $arrValues['address'] . ' 13230';
    
} else {
    
    $arrValues['status_code'] = 1;
    $arrValues['status_description'] = 'This HTTP Method is not supported';

}

header('Content-Type: application/json');
echo json_encode($arrValues);